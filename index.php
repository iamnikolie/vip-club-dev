<html>

    <head>
        <title>VIP Club Premier Palace - Головна</title>

        <meta charset="utf-8">

        <link rel="stylesheet" href="css/main.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">


        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100italic,100,300,300italic,400italic,500,500italic,700,700italic,900,900italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

    </head>

<body>

<script src="js/main.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <div class="container">

        <div class="row">
            <div class="col-md-12 text-center">
                <img class="logo-img" src="img/logo.png" alt="">
                <h1 class="text-center logo">
                    Сайт на реконструкції
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <hr>
            </div>
            git init
            git remote add origin https://iamnikolie@bitbucket.org/iamnikolie/vip-club-dev.git
        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                Для того, щоби переглянути офіційну інформацію - <strong>річний звіт за 2015 рік</strong>, <a href="#" data-toggle="modal" data-target="#info-modal">натисніть на посилання</a>
            </div>
        </div>


        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                &copy; 2016 <a href="http://vip-club.com.ua/">VIP Club Premier Palace</a>
            </div>
        </div>


    </div>

<div class="modal fade" id="info-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Річний звіт - 2015 рік (поданий 2016 року)</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <i class="fa fa-file-text" style="font-size: 40px;" aria-hidden="true"></i>
                        <br>
                        <br>
                        <a href="files/Report.xml">Звіт у затвердженому форматі *.xml</a>
                    </div>
                    <div class="col-md-4 text-center">
                        <i class="fa fa-file-word-o" style="font-size: 40px;" aria-hidden="true"></i>
                        <br>
                        <br>
                        <a href="files/zvit.doc">Звіт у форматі *.doc</a>
                    </div>
                    <div class="col-md-4 text-center">
                        <i class="fa fa-file-word-o" style="font-size: 40px;" aria-hidden="true"></i>
                        <br>
                        <br>
                        <a href="files/audit.doc">Аудиторський висновок у форматі *.doc</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрити вікно</button>
            </div>
        </div>
    </div>
</div>

</body>

</html>